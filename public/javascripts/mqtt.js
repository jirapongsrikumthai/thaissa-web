    var headTopic = "00152/01/";
    client = new Paho.MQTT.Client("soar.gistda.or.th", Number(9001), "client_id_"+String(Math.floor(Math.random() * 100000000000)));
    client.startTrace();
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    client.connect({
        onSuccess: onConnect,
        useSSL: true
    });
    console.log("attempting to connect...")
    // called when the client connects
    function onConnect() {
        console.log("onConnect");
        client.subscribe("web/00152/01/telescope");
        client.subscribe("web/00152/01/scopedome");
        client.subscribe("web/00152/01/shutter");
        client.subscribe("web/00152/01/setting");
        client.subscribe("web/00152/01/json");
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
        }
    }

    // called when a message arrives
    function onMessageArrived(message) {
        var obj = JSON.parse(message.payloadString);
        console.log(obj);
        outputTerminal(obj);
    }
    function sendMQTT(topic, data){
        
        var myJson = JSON.stringify(data);
        var msg = new Paho.MQTT.Message(myJson)
        msg.destinationName = headTopic + topic
        console.log(data);
        msg.qos = 2
        client.send(msg)
    }